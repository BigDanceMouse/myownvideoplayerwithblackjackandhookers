//
//  MOVideoView.m
//  MyOwnVideoPlayerWithBlackjackAndHookers
//
//  Created by Владимир Елизаров on 24.09.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

#import "MOVideoView.h"
#import <AVFoundation/AVFoundation.h>


@implementation MOVideoView
- (AVPlayer *)player {
    return self.playerLayer.player;
}

- (void)setPlayer:(AVPlayer *)player {
    self.playerLayer.player = player;
}

+ (Class)layerClass {
    return [AVPlayerLayer class];
}

- (AVPlayerLayer *)playerLayer {
    return (AVPlayerLayer *)self.layer;
}


@end
