//
//  MOVideoView.h
//  MyOwnVideoPlayerWithBlackjackAndHookers
//
//  Created by Владимир Елизаров on 24.09.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AVPlayer;
@class AVPlayerLayer;

@interface MOVideoView : UIView
@property AVPlayer *player;
@property (readonly) AVPlayerLayer *playerLayer;
@end
