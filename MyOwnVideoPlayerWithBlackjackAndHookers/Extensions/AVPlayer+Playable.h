//
//  MOAVPlayer+Playable.h
//  MyOwnVideoPlayerWithBlackjackAndHookers
//
//  Created by Владимир Елизаров on 23.09.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

#import "MOReversePlayable.h"

@interface AVPlayer(Playable) <ReversePlayable>

@end
