//
//  MOAVPlayer+Playable.m
//  MyOwnVideoPlayerWithBlackjackAndHookers
//
//  Created by Владимир Елизаров on 23.09.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

#import "AVPlayer+Playable.h"


const CGFloat kStraightRate = 1.0;
const CGFloat kReverseRate = -1.0;
const CGFloat kPauseRate = 0.0;

@implementation AVPlayer(Playable)

- (void) straightPlay {
    self.rate = kStraightRate;
}

- (void) reversePlay {
    self.rate = kReverseRate;
}

- (void) pause {
    self.rate = kPauseRate;
}

- (void) playFromStart {
    [self seekToTime:kCMTimeZero completionHandler:^(BOOL finished) {
        [self play];
    }];
}

- (void) playFromEnd {
    CMTime endTime = [[self currentItem] duration];
    [self seekToTime:endTime completionHandler:^(BOOL finished) {
        self.rate = kReverseRate;
    }];
}


@end
