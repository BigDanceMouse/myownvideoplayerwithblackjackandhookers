//
//  MOTransitionAssistent.m
//  MyOwnVideoPlayerWithBlackjackAndHookers
//
//  Created by Владимир Елизаров on 22.09.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

#import "MOTransitionAssistent.h"

@implementation MOTransitionAssistent

+ (instancetype)newDefaultAssistentWithSourceVC:(UIViewController*)source andDestenationVC:(UIViewController*)destenation {
    MOTransitionAssistent *assist = [MOTransitionAssistent new];
    destenation.transitioningDelegate = assist;
    return assist;
}

- (NSTimeInterval)transitionDuration:(nullable id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.3;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    UIViewController *sourceVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *destVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    CGRect destRect = [transitionContext finalFrameForViewController:destVC];
    
    destVC.view.frame = destRect;
    
    UIView *containterView = [transitionContext containerView];
    [containterView addSubview:sourceVC.view];
    
    destVC.view.transform = CGAffineTransformMakeScale(2, 0.01);
    destVC.view.alpha = 0;
    [containterView addSubview:destVC.view];
    
    NSTimeInterval duration = [self transitionDuration:transitionContext];
    
    
    [UIView animateKeyframesWithDuration:duration delay:UIViewKeyframeAnimationOptionCalculationModeLinear options: 0 animations:^{
        
        [UIView addKeyframeWithRelativeStartTime:0 relativeDuration: 0.5 animations:^{
            sourceVC.view.transform = CGAffineTransformMakeRotation(M_PI * 2);
        }];
        
        [UIView addKeyframeWithRelativeStartTime:0.5 relativeDuration:0 animations:^{
            destVC.view.alpha = 1;
        }];
        
        [UIView addKeyframeWithRelativeStartTime:0.5 relativeDuration: 0.5 animations:^{
            [containterView addSubview:destVC.view];
            destVC.view.transform = CGAffineTransformIdentity;
        }];
        
    } completion:^(BOOL finished) {
        dispatch_async(dispatch_get_main_queue(), ^{
            sourceVC.view.transform = CGAffineTransformIdentity;
        });
        [transitionContext completeTransition:YES];
    }];
    
}


#pragma mark UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    return self;
}

@end
