//
//  MOTransitionAssistent.h
//  MyOwnVideoPlayerWithBlackjackAndHookers
//
//  Created by Владимир Елизаров on 22.09.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MOTransitionAssistent : NSObject <UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning>

+ (instancetype)newDefaultAssistentWithSourceVC:(UIViewController*)source andDestenationVC:(UIViewController*)destenation;

@end
