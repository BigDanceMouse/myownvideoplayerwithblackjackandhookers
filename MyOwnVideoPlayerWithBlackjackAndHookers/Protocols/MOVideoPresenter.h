//
//  MOVideoPresenter.h
//  MyOwnVideoPlayerWithBlackjackAndHookers
//
//  Created by Владимир Елизаров on 24.09.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//


@protocol MOVideoPresenter

- (void) playVideoFromPath:(NSString *)path;

@end
