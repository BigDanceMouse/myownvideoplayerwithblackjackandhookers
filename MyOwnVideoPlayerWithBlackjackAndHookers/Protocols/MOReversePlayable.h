//
//  MOReversePlayable.h
//  MyOwnVideoPlayerWithBlackjackAndHookers
//
//  Created by Владимир Елизаров on 23.09.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

@protocol ReversePlayable

- (void) straightPlay;
- (void) reversePlay;
- (void) pause;
- (void) playFromStart;
- (void) playFromEnd;

@end
