//
//  ViewController.m
//  MyOwnVideoPlayerWithBlackjackAndHookers
//
//  Created by Владимир Елизаров on 21.09.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

#import "ViewController.h"
#import "MOTransitionAssistent.h"
#import "MOVideoPresenter.h"

@interface ViewController ()
{
    MOTransitionAssistent *transitionAssistent;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (NSString *)pathToVideo {
    NSBundle *bundle = [NSBundle mainBundle];
    NSString *path = [bundle pathForResource:@"video" ofType:@"mp4"];
    
    NSAssert(path && ![path isEqualToString:@""], @"there is no video in bundle. Re-implement logic or insert video file using name:video and type:mp4");
    
    return path;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    UIViewController *destenationVC = segue.destinationViewController;
    transitionAssistent = [MOTransitionAssistent newDefaultAssistentWithSourceVC: self
                                                                andDestenationVC: destenationVC];

    if([destenationVC.class conformsToProtocol: @protocol(MOVideoPresenter)]) {
        [(id<MOVideoPresenter>)destenationVC playVideoFromPath:[self pathToVideo]];
    }
}




@end
