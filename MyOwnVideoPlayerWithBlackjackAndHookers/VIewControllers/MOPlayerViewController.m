//
//  MOPlayerViewController.m
//  MyOwnVideoPlayerWithBlackjackAndHookers
//
//  Created by Владимир Елизаров on 21.09.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

#import "MOPlayerViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "MOVideoView.h"
#import "MOStateMachine.h"
#import "AVPlayer+Playable.h"

@interface MOPlayerViewController () {
    MOStateMachine *state; 
}

@end

@implementation MOPlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!state) {
        state = [MOStateMachine make];
    }
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self play];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscapeLeft;
}



- (IBAction)longTapGestureDidRecognized:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)doubleTapGestureDidRecognized:(id)sender {
    [self toggleDirection];
}



-(AVPlayer *)player {
    return [(MOVideoView *)self.view player];
}


- (void) playVideoFromPath:(NSString *)path {
    MOVideoView* _playerView = (MOVideoView *)self.view;
    
    NSURL *videoURL = [NSURL fileURLWithPath:path];
    NSAssert(videoURL, @"path must be the correct path to video");
    
    _playerView.player = [[AVPlayer alloc] initWithURL:videoURL];
    _playerView.player.actionAtItemEnd = AVPlayerActionAtItemEndNone;
                                          
    [self setupObserversesWithPlayer:_playerView.player];
}

-(void)setupObserversesWithPlayer: (AVPlayer *)player {
    NSAssert(player, @"player cannot be nil");
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(playerDidEndPlayingNotification:)
                                                     name: AVPlayerItemDidPlayToEndTimeNotification
                                                   object: [player currentItem]
         ];
        
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(pause)
                                                     name: UIApplicationWillResignActiveNotification
                                                   object: nil
         ];
        
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(play)
                                                     name: UIApplicationDidBecomeActiveNotification
                                                   object: nil
         ];
    });
}


-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)playerDidEndPlayingNotification:(NSNotification *)notification {
    [self beginPlaying];
}

-(void)beginPlaying {
    state = [state setToFirstPositionAndPlay:[self player]];
}

-(void)play {
    state = [state playOnPlayerAndMakeNewState:[self player]];
}

-(void)pause {
    state = [state pauseOnPlayerAndMakeNewState:[self player]];
}

-(void)toggleDirection {
    state = [state toggleDirectionOnPlayerAndMakeNewState:[self player]];
}


@end
