//
//  MOPlayerViewController.h
//  MyOwnVideoPlayerWithBlackjackAndHookers
//
//  Created by Владимир Елизаров on 21.09.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MOVideoPresenter.h"

@interface MOPlayerViewController : UIViewController <MOVideoPresenter>

@end
