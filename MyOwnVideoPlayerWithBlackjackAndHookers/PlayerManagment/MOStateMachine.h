//
//  MOStateMachine.h
//  MyOwnVideoPlayerWithBlackjackAndHookers
//
//  Created by Владимир Елизаров on 22.09.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MOReversePlayable.h"



@interface MOStateMachine : NSObject
+ (MOStateMachine*) make;

- (MOStateMachine*) playOnPlayerAndMakeNewState:(id<ReversePlayable>)player;
- (MOStateMachine*) pauseOnPlayerAndMakeNewState:(id<ReversePlayable>)player;
- (MOStateMachine*) toggleDirectionOnPlayerAndMakeNewState:(id<ReversePlayable>)player;

- (MOStateMachine*) setToFirstPositionAndPlay:(id<ReversePlayable>)player;

@end


@interface MOStraightState: MOStateMachine
@end

@interface MOReverseState: MOStateMachine
@end

@interface MOStraightPlayingState: MOStraightState
@end

@interface MOReversePlayingState: MOReverseState
@end

@interface MOStraightPausedState: MOStraightState
@end

@interface MOReversePausedState: MOReverseState
@end


