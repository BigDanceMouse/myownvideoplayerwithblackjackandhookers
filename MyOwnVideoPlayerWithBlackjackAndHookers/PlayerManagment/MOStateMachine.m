//
//  MOStateMachine.m
//  MyOwnVideoPlayerWithBlackjackAndHookers
//
//  Created by Владимир Елизаров on 22.09.17.
//  Copyright © 2017 Владимир Елизаров. All rights reserved.
//

#import "MOStateMachine.h"




@implementation MOStateMachine

+(instancetype) make {
    return [MOStraightPlayingState new];
}

- (instancetype) playOnPlayerAndMakeNewState:(id<ReversePlayable>)player {
    [self setToFirstPositionAndPlay:player];
    return self;
}

- (instancetype) pauseOnPlayerAndMakeNewState:(id<ReversePlayable>)player {
    return self;
}

- (instancetype) toggleDirectionOnPlayerAndMakeNewState:(id<ReversePlayable>)player {
    return self;
}

-(MOStateMachine*)setToFirstPositionAndPlay:(id<ReversePlayable>)player {
    return self;
}

@end



@implementation MOReverseState

-(MOStateMachine*)setToFirstPositionAndPlay:(id<ReversePlayable>)player {
    [player playFromEnd];
    return [MOReversePlayingState new];
}

@end


@implementation MOStraightState
    
-(MOStateMachine*)setToFirstPositionAndPlay:(id<ReversePlayable>)player {
    [player playFromStart];
    return [MOStraightPlayingState new];
}

@end


@implementation MOStraightPlayingState

- (MOStateMachine*) pauseOnPlayerAndMakeNewState:(id<ReversePlayable>)player {
    [player pause];
    return [[MOStraightPausedState alloc] init];
}

- (MOStateMachine*) toggleDirectionOnPlayerAndMakeNewState:(id<ReversePlayable>)player {
    [player reversePlay];
    return [MOReversePlayingState new];
}

@end



@implementation MOReversePlayingState

- (MOStateMachine*) pauseOnPlayerAndMakeNewState:(id<ReversePlayable>)player {
    [player pause];
    return [MOReversePausedState new];
}

- (MOStateMachine*) toggleDirectionOnPlayerAndMakeNewState:(id<ReversePlayable>)player {
    [player straightPlay];
    return [MOStraightPlayingState new];
}

@end



@implementation MOStraightPausedState

- (MOStateMachine*) playOnPlayerAndMakeNewState:(id<ReversePlayable>)player {
    [player straightPlay];
    return [MOStraightPlayingState new];
}

- (MOStateMachine*) pauseOnPlayerAndMakeNewState:(id<ReversePlayable>)player {
    [player pause];
    return [MOStraightPausedState new];
}

- (MOStateMachine*) toggleDirectionOnPlayerAndMakeNewState:(id<ReversePlayable>)player {
    return [MOReversePausedState new];
}

@end



@implementation MOReversePausedState

- (MOStateMachine*) playOnPlayerAndMakeNewState:(id<ReversePlayable>)player {
    [player reversePlay];
    return [MOReversePlayingState new];
}

- (MOStateMachine*) pauseOnPlayerAndMakeNewState:(id<ReversePlayable>)player {
    [player pause];
    return [MOReversePausedState new];
}

- (MOStateMachine*) toggleDirectionOnPlayerAndMakeNewState:(id<ReversePlayable>)player {
    [player pause];
    return [MOStraightPausedState new];
}

@end
